# hszg-w3-2023

In diesem Repository finden die Studenten der [HSZG](https://hszg.de) das Beispielprojekt,
welches in der Lehrveranstaltung für das Erklären der verschiedenen Technologien genutzt wird.

# Bausteine des Repositories

## Issues

In den Issues sind Beispielaufgaben zu finden. 

## Source: Backend

Der Spring Boot Server, welcher am 18.10.2023 mit den Studenten implementiert wurde.
