package com.cgi.hszg.springdemo.web.model;

public record UpdateUser(
        String name
) {
}
