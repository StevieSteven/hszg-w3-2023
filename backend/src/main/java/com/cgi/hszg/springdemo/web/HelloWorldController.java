package com.cgi.hszg.springdemo.web;

import com.cgi.hszg.springdemo.web.model.HelloUser;
import com.cgi.hszg.springdemo.web.model.UpdateUser;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
public class HelloWorldController {

    private String username = "not given";

    @GetMapping("/api/hello/{id}")
    public HelloUser helloWorld(@PathVariable String id) {
        return new HelloUser(
                username,
                "Hallo lieber " + username,
                LocalDateTime.now()
        );
    }

    @PostMapping("/api/hello")
    public void setName(@RequestBody UpdateUser newUser) {
        username = newUser.name();
    }

}
