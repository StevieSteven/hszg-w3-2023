package com.cgi.hszg.springdemo.domains.todo.model;

import java.time.LocalDateTime;
import java.util.UUID;

public record TodoEntry(
        UUID id,
        String name,
        String description,
        LocalDateTime createdAt
) {
}
