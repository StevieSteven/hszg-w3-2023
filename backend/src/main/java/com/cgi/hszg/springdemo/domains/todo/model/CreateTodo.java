package com.cgi.hszg.springdemo.domains.todo.model;

public record CreateTodo(
        String name,
        String description
) {
}
