package com.cgi.hszg.springdemo.web;

import com.cgi.hszg.springdemo.domains.todo.TodoService;
import com.cgi.hszg.springdemo.domains.todo.model.CreateTodo;
import com.cgi.hszg.springdemo.domains.todo.model.TodoEntry;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/todo")
public class TodoListController {

    private final TodoService todoService;

    public TodoListController(TodoService todoService) {
        this.todoService = todoService;
    }


    // gib mir alle Einträge zurück
    @GetMapping
    public List<TodoEntry> getTodos() {
        return todoService.getTodos();
    }

    // gibt mir einen bestimmten Eintrag zurück

    @GetMapping("/{id}")
    public TodoEntry getTodo(@PathVariable("id") UUID id) {
        return todoService
                .getTodoById(id)
                .orElseThrow();
    }

    // lege einen neuen Eintrag an
    @PostMapping
    public TodoEntry createTodo(@RequestBody CreateTodo todoInput) {
     return todoService.addTodoEntry(todoInput);
    }

    // aktualisiere einen Eintrag
    @PutMapping("/{id}")
    public TodoEntry updateTodo(@PathVariable("id") UUID id, @RequestBody TodoEntry todo) {
     return null;
    }

    // lösche einen Eintrag
    @DeleteMapping("/{id}")
    public void deleteTodo(UUID id)  {

    }
}
