package com.cgi.hszg.springdemo.domains.todo.db;

import com.cgi.hszg.springdemo.domains.todo.db.entities.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TodoRepository extends JpaRepository<TodoEntity, UUID> {

    List<TodoEntity> findAllByName(String name);
}
