package com.cgi.hszg.springdemo.domains.todo;

import com.cgi.hszg.springdemo.domains.todo.db.TodoRepository;
import com.cgi.hszg.springdemo.domains.todo.db.entities.TodoEntity;
import com.cgi.hszg.springdemo.domains.todo.db.mapper.TodoMapper;
import com.cgi.hszg.springdemo.domains.todo.model.CreateTodo;
import com.cgi.hszg.springdemo.domains.todo.model.TodoEntry;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private final TodoRepository todoRepository;
    private final TodoMapper todoMapper;

    public TodoService(TodoRepository todoRepository, TodoMapper todoMapper) {
        this.todoRepository = todoRepository;
        this.todoMapper = todoMapper;
    }

    public List<TodoEntry> getTodos() {
        return this.todoRepository.findAll().stream().map(todoMapper::toDTO).collect(Collectors.toList());
    }

    public Optional<TodoEntry> getTodoById(UUID id) {
        return this.todoRepository.findById(id).map(todoMapper::toDTO);
    }

    public TodoEntry addTodoEntry(CreateTodo createTodo) {
        TodoEntity newTodo = new TodoEntity(
                UUID.randomUUID(),
                createTodo.name(),
                createTodo.description(),
                LocalDateTime.now()
        );

        this.todoRepository.save(newTodo);

//        todos.add(newTodo);
        return this.todoMapper.toDTO(newTodo);
    }
}
