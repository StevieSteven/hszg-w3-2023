package com.cgi.hszg.springdemo.domains.todo.db.mapper;

import com.cgi.hszg.springdemo.domains.todo.db.entities.TodoEntity;
import com.cgi.hszg.springdemo.domains.todo.model.TodoEntry;
import org.springframework.stereotype.Component;

@Component
public class TodoMapper {

    public TodoEntry toDTO(TodoEntity entity) {
        return new TodoEntry(
                entity.getId(),
                entity.getName(),
                entity.getDescription(),
                entity.getCreatedAt()
        );
    }
}
